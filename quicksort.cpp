#include <iostream>
#include "Timer.h"

using namespace std;

// Funkcja Pivot - ustala wartosc srodkowa
int Pivot(int l, int r)
{
    return l+(r-l)/2;
}

//Funkcja zamieniajaca 2 elementy w tablicy //swap
void Zamien(int tab[], int i1, int i2)
{
    int temp=tab[i1];
    tab[i1]=tab[i2];
    tab[i2]=temp;
}

//Wybiera element wedlug ktorego ma byc podzielona tablica, nastepnie przerzuca mniejsze elementy od niej na lewo, a wieksze na prawo
int PodzielTablice(int tab[], int l, int r)
{
    int akt_pozycja=l;
    int pivot_ind=Pivot(l, r);
    int pivot_value=tab[pivot_ind];
    Zamien(tab, pivot_ind, r);

    for(int i=l;i<r;i++)
    {
        if (tab[i]<pivot_value)
        {
            Zamien(tab, i, akt_pozycja);
            akt_pozycja++;
        }
    }
    Zamien(tab, akt_pozycja, r);
    return akt_pozycja;
}

void Quicksort(int tab[], int l, int r)
{
    if (l<r)
    {
        //wybieram element ktory podzieli tablice, nastepnie rekurencyjnie sortuje kazda lewa i prawa podtablice, az do jednoel. tablic
        int i=PodzielTablice(tab, l, r);
        Quicksort(tab,l,i);
        Quicksort(tab,i+1,r);
    }
}


// Funkcja pomocnicza do odwracania tablic
void reverse_array(int tab1[], int rozmiar)
{

    int temp;
    for(int i = 0; i < rozmiar/2; i++)
    {
        temp = tab1[rozmiar-i-1];
        tab1[rozmiar-i-1] = tab1[i];
        tab1[i] = temp;
    }
   /* for(int j=0;j<rozmiar; j++)
    {
        cout << tab1[j] << " ";
    }*/
}


int main()
{

    Timer timer;
    int *tab2,n;
    double srednia=0 ,miedzyczas=0;
	cout<< "Podaj rozmiar tablicy: ";
	cin >> n;
	tab2 = new int[n];
	cout << endl;


for(int p=0;p<100;p++)
{
    for(int i=0;i<n;i++)
    {
        tab2[i]=(rand() );
      //  cout<<tab2[i]<<" ";
    }


    //Quicksort(tab2, 0,n*0.997);
   // reverse_array(tab2, n);
    timer.start();
    Quicksort(tab2,0,n-1);
    timer.stop();
    miedzyczas=timer.getElapsedTimeInMilliSec();


    cout<<"\nCzas " <<p+1<< ". proby: "<< miedzyczas << " milisekund "<< "\n";
    srednia+=miedzyczas;
}

    /*for(int i=0;i<n;i++)
        cout<<tab2[i]<<" ";*/

        cout<<endl<<endl;

        //cout << timer.getElapsedTimeInMilliSec() << " ms.\n";
        cout<<"srednia: "<<srednia/100<<" milisekund "<<endl;

    return 0;
}
