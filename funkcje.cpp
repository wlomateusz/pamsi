
#include <iostream>
#include <iostream>
#include <cstdlib>
#include <string>
#include <cstdio>
#include <conio.h>
#include "funkcje.h"

using namespace std;


//rysowanie planszy do gry
void GRA::plansza_do_gry(char t[])
{
  for(int i = 1; i <= rozmiar_kwadrat; i++)
  {
    cout << " " << t[i] << " ";
    if(i % rozmiar_planszy)       cout << "|";
    else if(i != rozmiar_kwadrat)
    {
        cout << "\n---+";

        for(int j = 1; j <= rozmiar_planszy - 2; j++) cout << "---+";

        cout << "---\n";
    }
    else cout << endl;
  }
}

//rozpoczecie gry
void GRA::Nowa_gra()
{
   char gracz,wybor;

   do
   {
       cout << "Podaj rozmiar planszy:\n";
       cin >> rozmiar_planszy;
       if (rozmiar_planszy<3) cout<< "Rozmiar planszy za maly"<< endl<<endl;
   } while(!(rozmiar_planszy > 2));

   rozmiar_kwadrat = rozmiar_planszy*rozmiar_planszy;

   char *t = new char [rozmiar_kwadrat + 1];

  do
  {
    cout << "Gra w Kolko i Krzyzyk" <<endl;
    cout << "Twoj znak to 'O'"<<endl<<endl;
    for(int i = 1; i <= rozmiar_kwadrat; i++) t[i] = ' ';
    gracz = 'O';

    while(!wygrana(t,'X',false) && !wygrana(t,'O',false) && !remis(t,false)) Ruch_Gracza_i_Zmiana(t,gracz);

    cout << "Jeszcze raz ? (T = TAK) : ";
    cin >> wybor;
    cout << endl;

  } while((wybor == 'T') || (wybor == 't'));

  delete [] t;
}

bool GRA::wygrana(char t[], char g, bool cisza)
{
  bool test = false;

  if(!test)
  {
      for(int i = 1; i <= rozmiar_planszy; i++)
      {
          test = true;
          for(int j = 0; j < rozmiar_planszy; j++)
            if(t[i+j*rozmiar_planszy] != g) test = false;

          if(test) break;
      }
  }

  if(!test)
  {
      for(int i = 1; i <= rozmiar_kwadrat; i+=rozmiar_planszy)
      {
          test = true;
          for(int j = 0; j < rozmiar_planszy; j++)
            if(t[i+j] != g) test = false;

          if(test) break;
      }
  }

  if(!test)
  {
      test = true;
      for(int i = 1; i <= rozmiar_kwadrat; i += rozmiar_planszy + 1)
      {
          if(t[i] != g) test = false;
      }
  }

  if(!test)
  {
      test = true;
      for(int i = rozmiar_planszy; i <= rozmiar_kwadrat - (rozmiar_planszy - 1); i += rozmiar_planszy -1 )
      {
          if(t[i] != g) test = false;
      }
  }

  if(test)
  {
    if(!cisza)
    {
      plansza_do_gry(t);
      cout << "\nGRACZ " << g << " WYGRYWA!!!\n\n";
    }
    return true;
  }
  return false;
}

bool GRA::remis(char t[], bool cisza)
{
  for(int i = 1; i <= rozmiar_kwadrat; i++) if(t[i] == ' ') return false;
  if(!cisza)
  {
    plansza_do_gry(t); cout << endl<<"remis"<<endl<<endl;
  }
  return true;
}

// Algorytm  Minimum_Maximum
// Algorytm otrzymuje plansze, na ktorej ustawione jest pole biezcego gracza. Parametr gracz przekazuje symbol gracza "o",
// a parametr wynik_w_przypadku_wygranej przekazuje jego wynik w przypadku wygranej.
int GRA::Minimum_Maximum(char t[], char gracz, int licznik)
{
  int m, wynik_w_przypadku_wygranej;

  if(wygrana(t,gracz,true)) return (gracz == 'X') ? 1 : -1;              // sprawdzam, czy gracz wygrywa na planszy. Jesli tak, to zwracam jego maks wynik.
  if(remis(t,true)) return 0;                                            // sprawdzam, czy  remis. Jezli jest, zwracam wynik 0

  gracz = (gracz == 'X') ? 'O' : 'X';                                    // zamieniam gracza na przeciwnika

  wynik_w_przypadku_wygranej = (gracz == 'O') ? 10 : -10;                // algorytm Minimum_Maximum w kolejnych wywolaniach rekurencyjnych naprzemiennie analizuje grê gracza oraz jego przeciwnika. Dla gracza oblicza maks wyniku gry, a dla przeciwnika oblicza min.

  for(int i = 1; i <= rozmiar_kwadrat; i++)                              // przegladam plansze i interesuja mnie wolne pola na ruch gracza. Na wolnym polu ustawiam symbol gracza i wyznaczamy wartosc tego ruchu rekurencyjnym wywolaniem algorytmu Minimum_Maximum.
    if(t[i] == ' ')                                                      // plansze przywracam i w zaleznosci kto gra: "X" to wyznaczam max, a jesli "O" to wyznaczam min
    {
       if(licznik == 0)
       {
           wynik_w_przypadku_wygranej = m;
           return wynik_w_przypadku_wygranej;
       }
       t[i] = gracz;
       m = Minimum_Maximum(t,gracz, licznik - 1);
       t[i] = ' ';
       if(((gracz == 'O') && (m < wynik_w_przypadku_wygranej)) || ((gracz == 'X') && (m > wynik_w_przypadku_wygranej))) wynik_w_przypadku_wygranej = m;
    }
  return wynik_w_przypadku_wygranej;
}

int GRA::Ruch_komputera(char t[])
{
  int ruch, i, m, wynik_w_przypadku_wygranej, licznik;

  wynik_w_przypadku_wygranej = -10;
  for(i = 1; i <= rozmiar_kwadrat; i++)
    if(t[i] == ' ')
    {
      t[i] = 'X';
      licznik = 9 - rozmiar_planszy;
      if(licznik < 3) licznik = 3;
      m = Minimum_Maximum(t,'X', licznik);
      t[i] = ' ';
      if(m > wynik_w_przypadku_wygranej)
      {
        wynik_w_przypadku_wygranej = m; ruch = i;
      }
    }

  return ruch;
}

void GRA::Ruch_Gracza_i_Zmiana(char t[], char &gracz)
{
  int r;

  plansza_do_gry(t);
 do
  {
      if(gracz == 'O')
      {
        cout << "\nCZLOWIEK : wybiera ruch : ";
        cin >> r;
        cout << endl;
      }
      else
      {
        r = Ruch_komputera(t);
        cout << "\nKOMPUTER : wybiera ruch : " << r << "\n" << endl;
      }
  }
  while(!((r >= 1) && (r <= rozmiar_kwadrat) && (t[r] == ' ')));
  t[r] = gracz;
  gracz = (gracz == 'O') ? 'X' : 'O';
}

bool GRA::Zakoncz()
{
    return Koniec;
}

bool GRA::Zakoncz_remisem()
{
    return Remis;
}
