
#include "main.h"
#include <cstdio>
#include <iostream>

CMenu *Menu;
GRA *Gra;

int main()
{
    Menu = new CMenu;
    Gra = new GRA;

    while(Menu->Zrob() != true)
    {
        Menu->Wyczysc();
        Menu->Pokaz_Menu();
        Menu->Czekaj_na_dane_wejsciowe();

        Menu->Wyczysc();

        if(Gra->Zakoncz() == true || Gra->Zakoncz_remisem() == true);

        if(Menu->Wybierz_opcje() == 1) Gra->Nowa_gra();
        if(Menu->Wybierz_opcje() == 2) Menu->Wyjscie_z_Gry();
    }

    delete Menu;
    Menu = NULL;

    delete Gra;
    Gra = NULL;

    return 0;
}
