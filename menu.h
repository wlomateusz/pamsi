
#ifndef MENU_H
#define MENU_H
#include <cstdlib>

class CMenu
{
    private:
        int Wybierz_pozycje_na_planszy;
        bool Wykonano;

    public:
        CMenu() { Wybierz_pozycje_na_planszy = 0; Wykonano = false; }
        ~CMenu() {};
        int Wybierz_opcje() { return Wybierz_pozycje_na_planszy; }
        bool Zrob(){ return Wykonano; };
        void Wyczysc() { system("cls"); };
        void Pokaz_Menu();
        void Czekaj_na_dane_wejsciowe();
        void Wyjscie_z_Gry(){ Wykonano = true;};
};
#endif
