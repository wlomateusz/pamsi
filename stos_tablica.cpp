

#include <iostream>

using namespace std;

template<typename Typ>
class stos
{
	Typ *arr;
	int height;
	int top;

public:
	stos(int h)
	{
		top = 0;
		arr = new Typ[top];
		height = h;
	}

	bool isEmpty(stos& S)
	{
		return S.top == 0;
	}

	bool isFull(stos& S)
	{
		return S.top == S.height;
	}

	bool push(stos& S, Typ el)
	{
		if (isFull(S))
		{
			cout << "Stos pelny, nie mozna dodac elementu" << endl << endl;
			return false;
		}
		S.arr[S.top++] = el;
		return true;
	}

	bool pop(stos<Typ>& S)
	{
		if (isEmpty(S))
			return false;
		S.arr[S.top] = S.arr[S.top--];
		return true;
	}
	void display()
	{
		cout << endl;
		for (int i = top - 1; i >= 0; i--)
		{
			if (arr[i] != 0)
				cout << arr[i] << endl;
		}
		cout << endl;
	}

	void clearStack(stos<Typ>& S)
	{
		for (int i = top - 1; i >= 0; i--)
			S.arr[S.top] = S.arr[S.top--];
	}

};

//---------------------------------------------------------------------------

int main()
{

	int opcja, H;
	int n;
	cout << "Podaj maksymalna wysokosc stosu: ";
	cin >> H;
	cout << endl;

	stos<int> S(H);

	do
	{
		cout << "-------------------------------------" << endl;
		cout << "1. Dodawanie elementu" << endl;
		cout << "2. Usuwanie elementu" << endl;
		cout << "3. Wyswietlanie zawartosci" << endl;
		cout << "4. Usuwanie wszystkich elementow" << endl;
		cout << "0. Koniec" << endl;
		cout << "-------------------------------------" << endl;
		cin >> opcja;
		switch (opcja)
		{
		case 1:
		{
			cout << endl << " Podaj liczbe ktora chcesz dodac do stosu: ";
			cin >> n;
			cout << endl;
			S.push(S, n);
			break;
		}

		case 2:
		{
			S.pop(S);
			cout << endl;
			break;
		}
		case 3:
		{
			cout << "\nStos:";
			S.display();
			break;
		}
		case 4:
		{
			S.clearStack(S);
			break;
		}

		}
	} while (opcja != 0);
	return 0;
}
