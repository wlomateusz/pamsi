#include <iostream>
#include <cstdlib>


using namespace std;


struct element
{
	int wartosc;
	element *nastepny;
};
class kolejka
{
    element *przod, *tyl;
    public:
    void dodaj(int x);
    void usun();
    void wyswietl();
    void usun_wszystkie();
    kolejka();
};

kolejka::kolejka()
{
    przod=NULL;
    tyl=NULL;
}


void kolejka::wyswietl()
{
    element *temp=new element;
    temp=przod;
    while(temp != NULL)
    {
        cout<<temp->wartosc<<" ";
        temp=temp->nastepny;
    }
}

void kolejka::dodaj(int x)
{
    element *temp=new element;
    temp->wartosc=x;
    temp->nastepny=NULL;
    if(przod==NULL && tyl==NULL)
    {
        przod=tyl=temp;
    }
    tyl->nastepny=temp;
    tyl=temp;
}

void kolejka::usun()
{
    element *temp=new element;
    temp=przod;
    if(przod==NULL)
    {
        cout<<"Kolejka jest pusta"<<endl;
    }
    if (przod==tyl)
        przod=tyl=NULL;
    else
        przod=przod->nastepny;
}

void kolejka::usun_wszystkie()
{
    if (przod==NULL) {przod=tyl=NULL; return;}
    else
    {
        element *temp=new element;
        temp=przod;
        przod=przod->nastepny;
        delete temp;
        usun_wszystkie();
    }
}

int main()
{
    kolejka Q;
    int n;
    int opcja;

do
{
cout << "-------------------------------------" << endl;
    cout<<"1. Dodawanie elementu do kolejki"<<endl;
    cout<<"2. Usuwanie elementu z kolejki"<<endl;
    cout<<"3. Wyswietlanie zawartosci kolejki"<<endl;
    cout<<"4. Usuwanie wszystkich elementow kolejki"<<endl;
    cout<<"0. Koniec"<<endl;
    cout << "-------------------------------------" << endl;
    cin>>opcja;
    cout << endl;
    switch(opcja)
    {
    case 1:
        {
            cout<<" Podaj liczbe ktora chcesz dodac do kolejki: "<< endl;
            cin>>n;
            Q.dodaj(n);
            break;
        }
    case 2:
        {
            Q.usun();
            break;
        }
    case 3:
        {
            cout << "Kolejka: ";
            Q.wyswietl();
            cout << endl;
            break;
        }
    case 4:
        {
            Q.usun_wszystkie();
            break;
        }
    }
}
while(opcja !=0);
    return 0;
}
