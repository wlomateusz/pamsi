#include <iostream>


using namespace std;

template<typename Typ>
class kolejka
{
	Typ *arr;
	int length;
	int first;
	int last;

public:
	kolejka(int l)
	{
		first = 0;
		last = 0;
		arr = new Typ[first];
		length = l;

	}

	bool isFull(kolejka& Q)
	{
		return(Q.first == 0 && Q.last == Q.length - 1) || (Q.first == Q.last + 1);
	}

	bool isEmpty(kolejka& Q)
	{
		return(Q.first == Q.last);
	}

	bool enqueue(kolejka& Q, Typ el)
	{
		if (isFull(Q))
		{
			cout << "Kolejka pelna, nie mozna dodac elementu" << endl << endl;
			return false;
		}
		Q.arr[Q.last++] = el;
		if (Q.last >= Q.length)
			Q.last = 0;
		return true;
	}

	bool dequeue(kolejka& Q)
	{
		if (isEmpty(Q))
		{
			cout << "Kolejka jest juz pusta, nie mozna usunac elementu" << endl;
			return false;
		}

		Q.arr[Q.first] = Q.arr[Q.first++];
		return true;
	}


	void display(kolejka& Q)
	{
		int i;
		for (i = Q.first; i<Q.last; i++)
		{
			if (isEmpty(Q)) return;
			else
				cout << Q.arr[i] << " ";

		}
		cout << endl;
	}

	void clearQueue(kolejka& Q)
	{

		for (int i = Q.last; i >= 0; i--)
			Q.arr[Q.first] = Q.arr[Q.first++];
		Q.first = Q.last = 0;
	}
};


//------------------------------------------------------------------


int main()
{
	int opcja, L;
	int n;
	cout << "Podaj rozmiar kolejki: ";
	cin >> L;
	cout << endl;
	kolejka<int> Q(L + 1);

	do
	{
		cout << "-------------------------------------" << endl;
		cout << "1. Dodawanie elementu" << endl;
		cout << "2. Usuwanie elementu" << endl;
		cout << "3. Wyswietlanie zawartosci" << endl;
		cout << "4. Usuwanie wszystkich elementow" << endl;
		cout << "0. Koniec" << endl;
		cout << "-------------------------------------" << endl;
		cin >> opcja;
		cout << endl;

		switch (opcja)
		{
		case 1:
		{
			cout << " Podaj liczbe ktora chcesz dodac do kolejki: ";
			cin >> n;
			cout << endl;
			Q.enqueue(Q, n);
			break;
		}

		case 2:
		{
			Q.dequeue(Q);
			cout << endl;
			break;
		}
		case 3:
		{
		    cout << "\nKolejka: ";
			Q.display(Q);
			break;
		}
		case 4:
		{
			Q.clearQueue(Q);
			break;
		}

		}
	} while (opcja != 0);
	return 0;
}
