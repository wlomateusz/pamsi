#include <iostream>
#include "Timer.h"

using namespace std;

// Funkcja tworzaca kopiec binarny, n - rozmiar kopca, i - wskaznik w tablicy
void ustawKopiec(int tab[], int n, int i)
{
    int najw = i;  // inicjalizuje najwieksza wartosc jako korzen
    int l=2*i+1;    //lewe dziecko 2i+1
    int r =2*i+2;   //prawe dziecko 2i+2

    // Jesli lewe dziecko jest wieksze od najwiekszego to staje sie najwieksze
    if (l<n && tab[l]>tab[najw])
        najw = l;

    // Jesli prawe dziecko jest wieksze od najwiekszego to staje sie najwieksze
    if (r<n && tab[r]>tab[najw])
        najw=r;

    // Jesli najwiekszy nie jest korzeniem zamienia sie miejscem z najwieksza wartoscia
    if (najw!=i)
    {
        swap(tab[i], tab[najw]);

        // Rekursywnie naprawiam obslugiwany kopiec
        ustawKopiec(tab, n, najw);
    }
}


void heapSort(int tab[], int n)
{

    // Tworzenie poprawnego kopca
    for (int i=n/2-1; i>=0; i--)
        ustawKopiec(tab, n, i);

    // Wstawianie elementow z kopca do tablicy
    for (int i=n-1; i>=0; i--)
    {
        swap(tab[0], tab[i]);      // Ustawiam korzen na koniec
        ustawKopiec(tab, i, 0);   // Naprawiam zredukowany kopiec
    }
}


// Funkcja pomocnicza do odwracania tablic
void reverse_array(int tab1[], int rozmiar)
{

    int temp;
    for(int i = 0; i < rozmiar/2; i++)
    {
        temp = tab1[rozmiar-i-1];
        tab1[rozmiar-i-1] = tab1[i];
        tab1[i] = temp;
    }
   /* for(int j=0;j<rozmiar; j++)
    {
        cout << tab1[j] << " ";
    }*/
}


int main()
{
    Timer timer;
    int *tab2,n;
    double miedzyczas=0, srednia=0;
	cout<< "Podaj rozmiar tablicy: ";
	cin >> n;
	tab2 = new int[n];
	cout << endl;

for(int p=0;p<100;p++)
{
    for(int i=0;i<n;i++)
    {
        tab2[i]=(rand());
        //cout<<tab2[i]<<" ";
    }

    //heapSort(tab2,n*0.99);
   // reverse_array(tab2, n);
    timer.start();
    heapSort(tab2,n);
    timer.stop();
    miedzyczas=timer.getElapsedTimeInMilliSec();


    cout<<"\nCzas " <<p+1<< ". proby: "<< miedzyczas << " milisekund "<< "\n";
    srednia+=miedzyczas;
}

    /*for(int i=0;i<n;i++)
        cout<<tab2[i]<<" ";*/

        cout<<endl<<endl;

        //cout << timer.getElapsedTimeInMilliSec() << " ms.\n";
        cout<<"srednia: "<<srednia/100<<" milisekund "<<endl;
    return 0;
}
