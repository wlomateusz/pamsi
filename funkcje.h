
#include <cstdlib>
#ifndef GRA_H
#define GAME_H
class GRA
{
    private:
        bool Koniec;
        bool Remis;
        int rozmiar_planszy;
        int rozmiar_kwadrat;

    public:

        GRA()
        { Koniec = false; Remis = false; };
        ~GRA() {};
        bool Zakoncz();
        bool Zakoncz_remisem();
        void Nowa_gra();
        void plansza_do_gry(char t[]);
        int Ruch_komputera(char t[]);
        void Ruch_Gracza_i_Zmiana(char t[], char &gracz);
        bool wygrana(char t[], char g, bool cisza);
        bool remis(char t[], bool cisza);
        int Minimum_Maximum(char t[], char gracz, int licznik);
};

#endif
