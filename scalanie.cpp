#include <iostream>
#include "Timer.h"

using namespace std;

int *t; //dodatkowa tablica
void scal(int tab[], int start, int mid, int kon)
{
    int i,j,q;
    for (i=start;i<=kon;i++)
        t[i]=tab[i];

    i=start;
    j=mid+1;
    q=start;

    while(i<=mid && j<=kon)
    {
        if(t[i]<t[j])
            tab[q++]=t[i++];
        else
            tab[q++]=t[j++];
    }

    while (i<=mid) tab[q++]=t[i++];

}

void SortScal(int tab[], int start, int kon)
{
    int mid;
    if(start<kon)
    {
        //wybieram element ktory podzieli tablice, nastepnie rekurencyjnie sortuje kazda lewa i prawa podtablice, az do jednoel. tablic
        mid=(start+kon)/2;
        SortScal(tab,start,mid);
        SortScal(tab,mid+1,kon);
        scal(tab,start,mid,kon);
    }
}

// Funkcja pomocnicza do odwracania tablic
void reverse_array(int *tab1, int rozmiar)
{

    int temp;
    for(int i = 0; i < rozmiar/2; i++)
    {
        temp = tab1[rozmiar-i-1];
        tab1[rozmiar-i-1] = tab1[i];
        tab1[i] = temp;
    }
   /* for(int j=0;j<rozmiar; j++)
    {
        cout << tab1[j] << " ";
    }*/
}

int main()
{
    Timer timer;
    int *tab2,n;
    double miedzyczas=0,srednia=0;
	cout<< "Podaj rozmiar tablicy: ";
	cin >> n;
	tab2 = new int[n];
	t = new int[n];
	cout << endl;


	for(int z=0;z<100;z++)
    {
    for(int i=0;i<n;i++)
    {
        tab2[i]=(rand());
        //cout<<tab2[i]<<" ";
    }


    //SortScal(tab2,0,n*0.5);
    //reverse_array(tab2, n);
    timer.start();
    SortScal(tab2,0,n-1);
    timer.stop();
    miedzyczas=timer.getElapsedTimeInMilliSec();

 cout<<"\nCzas " <<z+1<< ". proby: "<< miedzyczas << " milisekund "<< "\n";
    srednia+=miedzyczas;
}

    /*for(int i=0;i<n;i++)
        cout<<tab2[i]<<" ";*/

        cout<<endl<<endl;

        //cout << timer.getElapsedTimeInMilliSec() << " ms.\n";
        cout<<"srednia: "<<srednia/100<<" milisekund "<<endl;
        delete[]t; delete[]tab2;
    return 0;
}

