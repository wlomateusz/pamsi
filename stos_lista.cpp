#include <iostream>
#include <cstdlib>


using namespace std;


struct element
{
	int wartosc;
	element *nastepny;
};
class stos
{
    element *przod, *tyl;
    public:
    void dodaj(int x);
    void usun();
    void wyswietl();
    void usun_wszystkie();
    stos();
};

stos::stos()
{
    przod=NULL;
    tyl=NULL;
}


void stos::dodaj(int x)
{
    element *temp=new element;
    temp->wartosc=x;
    temp->nastepny=przod;
    przod=temp;
}

void stos::wyswietl()
{
    element *temp=new element;
    temp=przod;
    while(temp!=NULL)
    {

        cout<<temp->wartosc<<"\n";
        temp=temp->nastepny;
    }
}

void stos::usun()
{
    if (przod==NULL) cout<< "Lista jest pusta"<<endl;
    else
{
       element *temp=new element;
       temp=przod;
       przod=przod->nastepny;
       delete temp;
}
}

void stos::usun_wszystkie()
{
    while(przod!=NULL)
    {
        element *temp=new element;
        temp=przod;
        przod=przod->nastepny;
        delete temp;
    }
}

int main()
{
    stos S;
    int n;
    int opcja;

do
{
    cout << endl << "-------------------------------------" << endl;
    cout<<"1. Dodawanie elementu na stos"<<endl;
    cout<<"2. Usuwanie elementu ze stosu"<<endl;
    cout<<"3. Wyswietlanie zawartosci stosu"<<endl;
    cout<<"4. Usuwanie wszystkich elementow stosu"<<endl;
    cout<<"0. Koniec"<<endl;
    cout << "-------------------------------------"<< endl;
    cin>>opcja;
    cout << endl;
    switch(opcja)
    {
    case 1:
        {
            cout<<" Podaj liczbe ktora chcesz dodac na stos: ";
            cin>>n;
            S.dodaj(n);
            break;
        }
    case 2:
        {


            S.usun();
            break;
        }
    case 3:
        {
            cout << "Stos:\n";
            S.wyswietl();
            break;
        }
    case 4:
        {
            S.usun_wszystkie();
            break;
        }
    }
}
while(opcja !=0);
    return 0;
}
